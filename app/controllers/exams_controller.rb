class ExamsController < ApplicationController
  def index
    @exams = Exam.all
  end

  def new
    @exam = Exam.new
  end

  def create
    @exam = Exam.new(student_params)
    @exam.save
    flash[:notice] = "Data Berhasil di Simpan."
    redirect_to exams_path
  end

  def show
    id = params[:id]
    @exam = Exam.find(id)
  end

  def edit
    id = params[:id]
    @exam = Exam.find(id)
  end

  def update
    id = params[:id]
    exam = Exam.find(id)
    exam.update(student_params)  
    flash[:notice] = "Data Berhasil di Update."
    redirect_to exam_path(id)
  end

  def destroy
    id = params[:id]
    exam = Exam.find(id)
    exam.destroy
    flash[:notice] = "Data Berhasil di Hapus."
    redirect_to exams_path
  end

  private

  def student_params
    params.require(:exam).permit(:title, :mapel, :duration, :nilai, :active_status, :level, :student_id)
  end
end
