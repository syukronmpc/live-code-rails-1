class StudentsController < ApplicationController
  def index
    @students = Student.all
  end

  def new
    @student = Student.new
    @kelas = Student.getKelas
  end

  def create
    @student = Student.new(student_params)
    @student.save
    flash[:notice] = "Data Berhasil di Simpan."
    redirect_to students_path
  end

  def show
    id = params[:id]
    @student = Student.find(id)
  end

  def edit
    id = params[:id]
    @student = Student.find(id)
    @kelas = Student.getKelas
  end

  def update
    id = params[:id]
    student = Student.find(id)
    student.update(student_params)  
    flash[:notice] = "Data Berhasil di Update."
    redirect_to student_path(id)
  end

  def destroy
    id = params[:id]
    student = Student.find(id)
    student.destroy
    flash[:notice] = "Data Berhasil di Hapus."
    redirect_to students_path
  end

  private

  def student_params
    params.require(:student).permit(:name, :username, :age, :kelas, :address, :city, :nik)
  end
end
