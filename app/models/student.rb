class Student < ApplicationRecord

  def self.getKelas 
    arrayKelas = ['Kelas X', 'Kelas XI', 'Kelas XII']
    result = []
    arrayKelas.each do |kelas|
      result.push([kelas, kelas])
    end
    result
  end
end
