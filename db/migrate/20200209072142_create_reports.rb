class CreateReports < ActiveRecord::Migration[6.0]
  def up
    create_table :reports do |t|
      t.string :title
      t.float :hasil
      t.integer :teacher_id
      t.string :student_id
      t.timestamp :date

      t.timestamps
    end
  end

  def down 
    drop_table :reports
  end
end
