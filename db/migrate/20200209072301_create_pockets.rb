class CreatePockets < ActiveRecord::Migration[6.0]

  def up
    create_table :pockets do |t|
      t.float :balance
      t.integer :student_id
      t.integer :teacher_id

      t.timestamps
    end
    add_index :pockets, [:student_id, :teacher_id], unique: true
  end

  def down 
    drop_table :pockets
  end
end
