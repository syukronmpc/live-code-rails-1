# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Teacher.create(nik: '112342348732', name: 'Guru 1', age: 28, kelas: 'Kelas X', mapel: 'Bahasa Indonesia')
Teacher.create(nik: '113491291235', name: 'Guru 2', age: 22, kelas: 'Kelas XI', mapel: 'Bahasa Indonesia')
Teacher.create(nik: '112778091236', name: 'Guru 3', age: 27, kelas: 'Kelas XII', mapel: 'Bahasa Indonesia')
Teacher.create(nik: '112987891237', name: 'Guru 4', age: 24, kelas: 'Kelas X', mapel: 'Matematika')
Teacher.create(nik: '112456891238', name: 'Guru 5', age: 25, kelas: 'Kelas XI', mapel: 'Matematika')
Teacher.create(nik: '112334891239', name: 'Guru 6', age: 26, kelas: 'Kelas XII', mapel: 'Matematika')